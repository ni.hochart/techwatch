package errors

type NotFound struct {
	Error string  `json:"error"`
	ErrorDescription string  `json:"error_description"`
	ErrorField string  `json:"error_field"`
}
