package utils

import (
	"database/sql"
	"fmt"
	"os"
	"techwatch/pkg/models"
)

func GetDBConnection() (*sql.DB, error) {
	conf := models.Configuration{
		User: os.Getenv("mysql_user"),
		Password: os.Getenv("mysql_password"),
		Host: os.Getenv("mysql_host"),
	}

	return sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:3306)/techwatch", conf.User, conf.Password, conf.Host))
}

