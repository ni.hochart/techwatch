package utils

import (
	"net/http"
	"strconv"
)


type Filter struct {
	PerPage     int  `schema:"per_page"`
	SortBy      string `schema:"sort_by"`
	CurrentPage int    `schema:"current_page"`
}


func ValidateFilter(r *http.Request) Filter {
	filter := Filter{
		PerPage:     50,
		SortBy:      "DESC",
		CurrentPage: 1,
	}

	if len(r.URL.Query().Get("per_page")) > 0 {
		perPage, _ := strconv.Atoi(r.URL.Query().Get("per_page"))
		if perPage > 0 {
			filter.PerPage = perPage
		}
	}

	if len(r.URL.Query().Get("current_page")) > 0 {
		currentPage, _ := strconv.Atoi(r.URL.Query().Get("current_page"))
		if currentPage > 0 {
			filter.CurrentPage = currentPage
		}
	}

	if len(r.URL.Query().Get("sort_by")) > 0 {
		sortBy := r.URL.Query().Get("sort_by")
		if sortBy == "ASC" {
			filter.SortBy = sortBy
		}
	}

	return filter
}

