package handlers

import (
	"database/sql"
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"techwatch/pkg/errors"
	"techwatch/pkg/models"
	"techwatch/pkg/utils"
)

func PostsList(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode("")
		return
	}

	countRows, err := db.Query("SELECT count(*) FROM post WHERE company_id=?", claims.CompanyId)
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}
	var count int
	for countRows.Next() {
		err = countRows.Scan(&count)
	}
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	filter := utils.ValidateFilter(r)

	results, err := db.Query(fmt.Sprintf("SELECT id, title, url, creation_date, metadata, active FROM post WHERE company_id=? AND active=1 ORDER BY creation_date %s LIMIT %d OFFSET %d", filter.SortBy, filter.PerPage, (filter.CurrentPage-1)*filter.PerPage), claims.CompanyId)
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	var posts []models.Post
	for results.Next() {
		var post models.Post
		var metadataJson []byte
		err = results.Scan(&post.Id, &post.Title, &post.Url, &post.CreationDate, &metadataJson, &post.Active)
		if err != nil {
			log.Println("Cannot retrieve MySQL data", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err := json.Unmarshal(metadataJson, &post.Meta)
		if err != nil {
			log.Println("cannot unMarshal metadata", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		models.AddCategoryToPost(db, &post)

		posts = append(posts, post)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(models.ListPost{
		Elements:     posts,
		CountElement: count,
		CurrentPage:  filter.CurrentPage,
		Sort:         filter.SortBy,
		PerPage:      filter.PerPage,
	})

	defer db.Close()
}

func PostsByTitleList(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode("")
		return
	}

	vars := mux.Vars(r)
	title := vars["title"]

	countRows, err := db.Query("SELECT count(*) FROM post WHERE company_id = ? AND c.title LIKE ?", claims.CompanyId, "%"+title+"%")
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}
	var count int
	for countRows.Next() {
		err = countRows.Scan(&count)
	}
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	filter := utils.ValidateFilter(r)

	results, err := db.Query(fmt.Sprintf("SELECT id, title, url, creation_date, metadata, active FROM post WHERE company_id=? ORDER BY creation_date %s LIMIT %d OFFSET %d", filter.SortBy, filter.PerPage, (filter.CurrentPage-1)*filter.PerPage), claims.CompanyId)
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	var posts []models.Post
	for results.Next() {
		var post models.Post
		var metadataJson []byte
		err = results.Scan(&post.Id, &post.Title, &post.Url, &post.CreationDate, &metadataJson, &post.Active)
		if err != nil {
			log.Println("Cannot retrieve MySQL data", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err := json.Unmarshal(metadataJson, &post.Meta)
		if err != nil {
			log.Println("cannot unMarshal metadata", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		models.AddCategoryToPost(db, &post)
		posts = append(posts, post)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(models.ListPost{
		Elements:     posts,
		CountElement: count,
		CurrentPage:  filter.CurrentPage,
		Sort:         filter.SortBy,
		PerPage:      filter.PerPage,
	})

	defer db.Close()
}

func PostsCategoryList(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode("")
		return
	}

	vars := mux.Vars(r)
	categoryId := vars["categoryId"]

	countRows, err := db.Query("SELECT count(*) FROM post p JOIN post_category pc ON pc.post_id=p.id JOIN category c ON pc.category_id=c.id WHERE c.id = ? AND active=1 AND p.company_id=?", categoryId, claims.CompanyId)
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}
	var count int
	for countRows.Next() {
		err = countRows.Scan(&count)
	}
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	filter := utils.ValidateFilter(r)

	results, err := db.Query(fmt.Sprintf("SELECT p.id, p.title, p.url, p.creation_date, p.metadata, p.active FROM post p JOIN post_category pc ON pc.post_id=p.id JOIN category c ON pc.category_id=c.id WHERE c.id = ? AND active=1 AND p.company_id=? ORDER BY creation_date %s LIMIT %d OFFSET %d", filter.SortBy, filter.PerPage, (filter.CurrentPage-1)*filter.PerPage), categoryId, claims.CompanyId)
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	var posts []models.Post
	for results.Next() {
		var post models.Post
		var metadataJson []byte
		err = results.Scan(&post.Id, &post.Title, &post.Url, &post.CreationDate, &metadataJson, &post.Active)
		if err != nil {
			log.Println("Cannot retrieve MySQL data", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		err := json.Unmarshal(metadataJson, &post.Meta)
		if err != nil {
			log.Println("cannot unMarshal metadata", err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		models.AddCategoryToPost(db, &post)
		posts = append(posts, post)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(models.ListPost{
		Elements:     posts,
		CountElement: count,
		CurrentPage:  filter.CurrentPage,
		Sort:         filter.SortBy,
		PerPage:      filter.PerPage,
	})

	defer db.Close()
}

func Posts(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	postId := vars["postId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(postId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing post id",
			ErrorField:       "post_id",
		})
		return
	}

	post, err := models.GetPost(db, postId, claims.CompanyId)
	if err != nil {
		if err == sql.ErrNoRows {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(errors.NotFound{
				Error:            "post not exist",
				ErrorDescription: fmt.Sprintf("post %s not found", postId),
				ErrorField:       "post_id",
			})

			return
		}
		log.Print(err.Error())
		// If the error is of any other type, send a 500 status
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(post)
	defer db.Close()
}

func PostsUpdate(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	postId := vars["postId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(postId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing post id",
			ErrorField:       "post_id",
		})
		return
	}
	if len(r.Form.Get("titre")) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing post titre",
			ErrorField:       "titre",
		})
		return
	}
	if len(r.Form.Get("url")) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing post url",
			ErrorField:       "url",
		})
		return
	}
	post, err := models.GetPost(db, postId, claims.CompanyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "post not exist",
			ErrorDescription: fmt.Sprintf("post %s not found", postId),
			ErrorField:       "post_id",
		})
		defer db.Close()
		return
	}

	models.ScrapeHtmlFromUrl(&post)
	active := 0
	if post.Meta != nil {
		active = 1
	}
	insForm, err := db.Prepare("UPDATE post SET titre=?, url=?, metadata=?, active=? where id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	jsonData, err := json.Marshal(post.Meta)

	insForm.Exec(r.Form.Get("titre"), r.Form.Get("url"), jsonData, active, post.Id)

	post, err = models.GetPost(db, postId, claims.CompanyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "post not exist",
			ErrorDescription: fmt.Sprintf("post %s not found", postId),
			ErrorField:       "post_id",
		})
		return
	}

	json.NewEncoder(w).Encode(post)
	defer db.Close()
}

func PostsCreate(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var postRequest models.CreationPost
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	if err := json.Unmarshal(body, &postRequest); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnprocessableEntity) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			defer db.Close()
			return
		}
	}

	post := models.Post{
		Title: postRequest.Title,
		Url:   postRequest.Url,
	}

	models.ScrapeHtmlFromUrl(&post)
	active := 0
	if post.Meta != nil {
		active = 1
	}

	log.Print(post.Meta)
	jsonData, err := json.Marshal(post.Meta)
	log.Print(jsonData)
	insForm, err := db.Prepare("INSERT INTO post(title, url, company_id, metadata, active) VALUES(?,?,?,?,?)")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}

	res, _ := insForm.Exec(postRequest.Title, postRequest.Url, claims.CompanyId, jsonData, active)
	id, _ := res.LastInsertId()
	post.Id = int(id)

	categoriesLinked := strings.Split(postRequest.Categories, ",")
	for categoryLinked := range categoriesLinked {
		var category models.Category
		err := db.QueryRow("SELECT id FROM category where company_id = ? AND label = ?", claims.CompanyId, categoryLinked).Scan(&category.Id)
		if err != nil && err != sql.ErrNoRows {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			db.Close()
			return
		}

		insCategoryPost, err := db.Prepare("INSERT INTO post_category(post_id, categeory_id) VALUES(?,?)")
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			db.Close()
			return
		}
		_, _ = insCategoryPost.Exec(post.Id, category.Id)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(post)
	defer db.Close()
}

func PostsDelete(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	postId := vars["postId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(postId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing post id",
			ErrorField:       "post_id",
		})
		defer db.Close()
		return
	}

	_, err = models.GetPost(db, postId, claims.CompanyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "post not exist",
			ErrorDescription: fmt.Sprintf("post %s not found", postId),
			ErrorField:       "post_id",
		})
		defer db.Close()
		return
	}

	stmtPostCat, err := db.Prepare("DELETE FROM post_category WHERE post_id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	_, _ = stmtPostCat.Exec(postId)

	stmt, err := db.Prepare("DELETE FROM post WHERE id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}

	_, _ = stmt.Exec(postId)
	w.WriteHeader(http.StatusNoContent)
	defer db.Close()
}
