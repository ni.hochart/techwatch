package handlers

import (
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"techwatch/pkg/errors"
	"techwatch/pkg/models"
	"techwatch/pkg/utils"
)

func CategoriesList(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode("")
		return
	}

	countRows, err := db.Query("SELECT count(*) FROM category")
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}
	var count int
	for countRows.Next() {
		err = countRows.Scan(&count)
	}
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	filter := utils.ValidateFilter(r)

	results, err := db.Query(fmt.Sprintf("SELECT id, label, creation_date FROM category WHERE company_id=? ORDER BY creation_date %s LIMIT %d OFFSET %d", filter.SortBy, filter.PerPage, (filter.CurrentPage-1)*filter.PerPage), claims.CompanyId)
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(nil)
		return
	}

	var categories []models.Category
	for results.Next() {
		var category models.Category
		err = results.Scan(&category.Id, &category.Label, &category.CreationDate)
		if err != nil {
			log.Fatal("Cannot retrieve MySQL data", err.Error())
		}
		categories = append(categories, category)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(models.ListCategory{
		Elements:     categories,
		CountElement: count,
		CurrentPage:  filter.CurrentPage,
		Sort:         filter.SortBy,
		PerPage:      filter.PerPage,
	})

	defer db.Close()
}

func Categories(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	categoryId := vars["categoryId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(categoryId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing category id",
			ErrorField:       "category_id",
		})
		return
	}

	category, err := models.GetCategory(db, categoryId, claims.CompanyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "category not exist",
			ErrorDescription: fmt.Sprintf("category %s not found", categoryId),
			ErrorField:       "category_id",
		})
		return
	}

	json.NewEncoder(w).Encode(category)
	defer db.Close()
}

func CategoriesUpdate(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	categoryId := vars["categoryId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(categoryId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing category id",
			ErrorField:       "category_id",
		})
		return
	}
	if len(r.Form.Get("category_name")) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing category label",
			ErrorField:       "label",
		})
		return
	}

	_, err = models.GetCategory(db, categoryId, claims.CompanyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "category not exist",
			ErrorDescription: fmt.Sprintf("category %s not found", categoryId),
			ErrorField:       "category_id",
		})
		defer db.Close()
		return
	}

	insForm, err := db.Prepare("UPDATE category SET label=? where id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	_, _ = insForm.Exec(r.Form.Get("label"), categoryId)

	category, err := models.GetCompany(db, categoryId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "category not exist",
			ErrorDescription: fmt.Sprintf("category %s not found", categoryId),
			ErrorField:       "category_id",
		})
		return
	}

	json.NewEncoder(w).Encode(category)
	defer db.Close()
}

func CategoriesCreate(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var category models.Category
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	if err := json.Unmarshal(body, &category); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnprocessableEntity) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			defer db.Close()
			return
		}
	}

	insForm, err := db.Prepare("INSERT INTO category(label, company_id) VALUES(?,?)")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}

	res, _ := insForm.Exec(category.Label, claims.CompanyId)
	id, _ := res.LastInsertId()
	category.Id = int(id)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(category)
	defer db.Close()
}

func CategoriesDelete(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	categoryId := vars["categoryId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(categoryId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing category id",
			ErrorField:       "category_id",
		})
		defer db.Close()
		return
	}

	_, err = models.GetCategory(db, categoryId, claims.CompanyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "category not exist",
			ErrorDescription: fmt.Sprintf("category %s not found", categoryId),
			ErrorField:       "category_id",
		})
		defer db.Close()
		return
	}

	stmtPostCat, err := db.Prepare("DELETE FROM post_category WHERE category_id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	_, _ = stmtPostCat.Exec(categoryId)

	deleteCategory, err := db.Prepare("DELETE FROM category WHERE id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}

	deleteCategory.Exec(categoryId)
	w.WriteHeader(http.StatusNoContent)
	defer db.Close()
}
