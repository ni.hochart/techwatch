package handlers

import (
	"database/sql"
	"encoding/json"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"net/http"
	"techwatch/pkg/models"
	"techwatch/pkg/utils"
	"time"
)

func Signup(w http.ResponseWriter, r *http.Request) {
	db, err := utils.GetDBConnection()
	// Parse and decode the request body into a new `models.User` instance
	user := &models.User{}
	err = json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		// If there is something wrong with the request body, return a 400 status
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// Salt and hash the password using the bcrypt algorithm
	// The second argument is the cost of hashing, which we arbitrarily set as 8 (this value can be more or less, depending on the computing power you wish to utilize)
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(user.Password), 8)

	// Next, insert the username, along with the hashed password into the database
	if _, err = db.Query("INSERT INTO user (username, password, company_id) VALUES (?, ?, ?)", user.Username, string(hashedPassword), user.CompanyId); err != nil {
		log.Printf(err.Error())
		// If there is any issue with inserting into the database, return a 500 error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	// We reach this point if the credentials we correctly stored in the database, and the default status of 200 is sent back
}

func Signin(w http.ResponseWriter, r *http.Request) {
	db, err := utils.GetDBConnection()
	// Parse and decode the request body into a new `models.User` instance
	user := &models.UserLogin{}
	err = json.NewDecoder(r.Body).Decode(user)
	if err != nil {
		log.Print(err.Error())
		// If there is something wrong with the request body, return a 400 status
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	// Get the existing entry present in the database for the given username
	result := db.QueryRow("SELECT username, password, company_id FROM user WHERE username=?", user.Username)
	// We create another instance of `models.User` to store the credentials we get from the database
	storedCreds := &models.ResponseUser{}
	// Store the obtained password in `storedCreds`
	err = result.Scan(&storedCreds.Username, &storedCreds.Password, &storedCreds.CompanyId)
	if err != nil {
		// If an entry with the username does not exist, send an "Unauthorized"(401) status
		if err == sql.ErrNoRows {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		log.Print(err.Error())
		// If the error is of any other type, send a 500 status
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Compare the stored hashed password, with the hashed version of the password that was received
	if err = bcrypt.CompareHashAndPassword([]byte(storedCreds.Password), []byte(user.Password)); err != nil {
		// If the two passwords don't match, return a 401 status
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	expirationTime := time.Now()
	// Create the JWT claims, which includes the username and expiry time
	claims := &models.Claims{
		Username:  storedCreds.Username,
		CompanyId: storedCreds.CompanyId,
		StandardClaims: jwt.StandardClaims{
			IssuedAt: expirationTime.Unix(),
		},
	}

	jwtKey, err := ioutil.ReadFile("server.pem")
	if err != nil {
		log.Printf(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err := token.SignedString(jwtKey)
	if err != nil {
		log.Print(err.Error())
		// If there is an error in creating the JWT return an internal server error
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	storedCreds.Token = tokenString

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	json.NewEncoder(w).Encode(storedCreds)
	defer db.Close()

}
