package handlers

import (
	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"techwatch/pkg/errors"
	"techwatch/pkg/models"
	"techwatch/pkg/utils"
)

func CompaniesList(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	countRows, err := db.Query("SELECT count(*) FROM company")
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var count int
	for countRows.Next() {
		err = countRows.Scan(&count)
	}
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	filter := utils.ValidateFilter(r)

	results, err := db.Query(fmt.Sprintf("SELECT id, name, creation_date FROM company ORDER BY creation_date %s LIMIT %d OFFSET %d", filter.SortBy, filter.PerPage, (filter.CurrentPage-1)*filter.PerPage))
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	var categories []models.Company
	for results.Next() {
		var company models.Company
		err = results.Scan(&company.Id, &company.Name, &company.CreationDate)
		if err != nil {
			log.Fatal("Cannot retrieve MySQL data", err.Error())
		}
		categories = append(categories, company)
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(models.ListCompany{
		Elements:     categories,
		CountElement: count,
		CurrentPage:  filter.CurrentPage,
		Sort:         filter.SortBy,
		PerPage:      filter.PerPage,
	})

	defer db.Close()
}

func Companies(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	companyId := vars["companyId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(companyId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing company id",
			ErrorField:       "company_id",
		})
		return
	}

	company, err := models.GetCompany(db, companyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "company not exist",
			ErrorDescription: fmt.Sprintf("company %s not found", companyId),
			ErrorField:       "company_id",
		})
		return
	}

	json.NewEncoder(w).Encode(company)
	defer db.Close()
}

func CompaniesUpdate(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	companyId := vars["companyId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(companyId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing company id",
			ErrorField:       "company_id",
		})
		return
	}
	if len(r.Form.Get("name")) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing company name",
			ErrorField:       "name",
		})
		return
	}

	insForm, err := db.Prepare("UPDATE company SET name=? where id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	insForm.Exec(r.Form.Get("name"), companyId)

	company, err := models.GetCompany(db, companyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "company not exist",
			ErrorDescription: fmt.Sprintf("company %s not found", companyId),
			ErrorField:       "company_id",
		})
		return
	}

	json.NewEncoder(w).Encode(company)
	defer db.Close()
}

func CompaniesCreate(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	var company models.Company
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	if err := r.Body.Close(); err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	if err := json.Unmarshal(body, &company); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(http.StatusUnprocessableEntity) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			defer db.Close()
			return
		}
	}

	insForm, err := db.Prepare("INSERT INTO company(name) VALUES(?)")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}

	res, err := insForm.Exec(company.Name)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}

	id, _ := res.LastInsertId()
	company.Id = int(id)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(company)
	defer db.Close()
}

func CompaniesDelete(w http.ResponseWriter, r *http.Request) {
	claims := EnsureAuth(w, r)
	if claims == nil {
		return
	}

	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	vars := mux.Vars(r)
	companyId := vars["companyId"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	if len(companyId) == 0 {
		w.WriteHeader(http.StatusNotFound)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing company id",
			ErrorField:       "company_id",
		})
		defer db.Close()
		return
	}

	_, err = models.GetCompany(db, companyId)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "company not exist",
			ErrorDescription: fmt.Sprintf("company %s not found", companyId),
			ErrorField:       "company_id",
		})
		defer db.Close()
		return
	}

	stmt, err := db.Prepare("DELETE FROM company WHERE id = ?")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}

	_, _ = stmt.Exec(companyId)
	w.WriteHeader(http.StatusNoContent)
	defer db.Close()
}
