package handlers

import (
	jwtmiddleware "github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"io/ioutil"
	"log"
	"net/http"
	"techwatch/pkg/models"
)

func EnsureAuth (w http.ResponseWriter, r *http.Request) *models.Claims {
	token, err := jwtmiddleware.FromAuthHeader(r)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return nil
	}

	claims, valid := extractClaims(token)
	if valid == false {
		w.WriteHeader(http.StatusUnauthorized)
		return nil
	}

	return claims
}


func getKeyFunc(token *jwt.Token) (interface{}, error) {
	key, err := ioutil.ReadFile("server.pem")
	if err != nil {
		return nil, err
	}
	return key, nil
}

func extractClaims(tokenStr string) (*models.Claims, bool) {
	token, err := jwt.Parse(tokenStr, getKeyFunc)

	if err != nil {
		return nil, false
	}

	claims := token.Claims.(jwt.MapClaims)

	tokenClaims := models.Claims{
		Username:       claims["username"].(string),
		CompanyId:      int(claims["company_id"].(float64)),
	}

	return &tokenClaims, token.Valid
}
