package handlers

import (
	"database/sql"
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"strings"
	"techwatch/pkg/errors"
	"techwatch/pkg/models"
	"techwatch/pkg/utils"
)

func PostsCreateFromSlack(w http.ResponseWriter, r *http.Request) {
	db, err := utils.GetDBConnection()
	if err != nil {
		log.Println("Cannot connect to the database", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	slackTokens, ok := r.URL.Query()["token"]

	if !ok || len(slackTokens[0]) < 1 {
		log.Println("Url Param 'token' is missing")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing token",
			ErrorField:       "token",
		})
		defer db.Close()
		return
	}

	company, err := models.GetCompanyForSlack(db, slackTokens[0])
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "unknown token",
			ErrorDescription: "wrong slack token",
			ErrorField:       "toke",
		})
		defer db.Close()
		return
	}

	keys, ok := r.URL.Query()["text"]

	if !ok || len(keys[0]) < 1 {
		log.Println("Url Param 'text' is missing")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing text",
			ErrorField:       "text",
		})
		defer db.Close()
		return
	}

	postInfo := keys[0]
	postInfoArray := strings.Split(postInfo, " ")
	postRequest := models.CreationPost{
		Title:      postInfoArray[0],
		Url:        postInfoArray[1],
		Categories: postInfoArray[2],
	}

	post := models.Post{
		Title: postInfoArray[0],
		Url:   postInfoArray[1],
	}
	models.ScrapeHtmlFromUrl(&post)
	active := 0
	if post.Meta != nil {
		active = 1
	}

	insForm, err := db.Prepare("INSERT INTO post(title, url, company_id, metadata, active) VALUES(?,?,?,?,?)")
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		defer db.Close()
		return
	}
	jsonData, err := json.Marshal(post.Meta)

	res, _ := insForm.Exec(postRequest.Title, postRequest.Url, company.Id, jsonData, active)
	id, _ := res.LastInsertId()
	post.Id = int(id)

	categoriesLinked := strings.Split(postRequest.Categories, ",")
	for _, categoryLinked := range categoriesLinked {
		var category models.Category
		err := db.QueryRow("SELECT id FROM category where company_id = ? AND label = ?", company.Id, categoryLinked).Scan(&category.Id)
		if err != nil && err != sql.ErrNoRows {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			db.Close()
			return
		}

		if err == sql.ErrNoRows {
			insCategory, err := db.Prepare("INSERT INTO category(label, company_id) VALUES(?,?)")
			if err != nil {
				log.Println(err.Error())
				w.WriteHeader(http.StatusInternalServerError)
				db.Close()
				return
			}

			resCategory, _ := insCategory.Exec(categoryLinked, company.Id)
			idCategory, _ := resCategory.LastInsertId()
			category, _ = models.GetCategory(db, string(idCategory), company.Id)
		}

		insCategoryPost, err := db.Prepare("INSERT INTO post_category(post_id, category_id) VALUES(?,?)")
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			db.Close()
			return
		}
		_, err = insCategoryPost.Exec(post.Id, category.Id)
		if err != nil {
			log.Println(err.Error())
			w.WriteHeader(http.StatusInternalServerError)
			db.Close()
			return
		}
	}

	post, err = models.GetPost(db, strconv.Itoa(post.Id), company.Id)
	if err != nil {
		log.Println(err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		db.Close()
		return
	}

	responseUrls, ok := r.URL.Query()["response_url"]

	if !ok || len(responseUrls[0]) < 1 {
		log.Println("Url Param 'response_url' is missing")
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(errors.NotFound{
			Error:            "not found",
			ErrorDescription: "missing response url",
			ErrorField:       "response_url",
		})
		defer db.Close()
		return
	}

	categoryText := ""
	for index, category := range post.Categories {
		categoryText += category.Label
		if index != len(post.Categories)-1 {
			categoryText += ","
		}
	}

	responseText := "New techwatch"
	if len(categoryText) > 0 {
		responseText += " in " + categoryText
	}
	responseText += " created " + post.Title + " " + post.Url

	slackResponse := models.SlackResponse{
		ResponseType: "in_channel",
		Text:         responseText,
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(slackResponse)
	defer db.Close()
}
