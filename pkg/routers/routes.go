package routers

import (
	"net/http"
	"techwatch/pkg/handlers"

	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

func NewRouter() *mux.Router {
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(route.HandlerFunc)
	}

	return router
}

var routes = Routes{
	Route{
		"Register",
		"POST",
		"/register",
		handlers.Signup,
	},
	Route{
		"Login",
		"POST",
		"/login",
		handlers.Signin,
	},
	Route{
		"CategoriesList",
		"GET",
		"/categories",
		handlers.CategoriesList,
	},
	Route{
		"CategoriesShow",
		"GET",
		"/categories/{categoryId}",
		handlers.Categories,
	},
	Route{
		"CategoriesUpdate",
		"PUT",
		"/categories/{categoryId}",
		handlers.CategoriesUpdate,
	},
	Route{
		"CategoriesCreate",
		"POST",
		"/categories",
		handlers.CategoriesCreate,
	},
	Route{
		"CategoriesDelete",
		"DELETE",
		"/categories/{categoryId}",
		handlers.CategoriesDelete,
	},
	Route{
		"PostsList",
		"GET",
		"/posts",
		handlers.PostsList,
	},
	Route{
		"PostsByTitleList",
		"GET",
		"/posts/search/{title}",
		handlers.PostsByTitleList,
	},
	Route{
		"PostsByCategoryList",
		"GET",
		"/posts/category/{categoryId}",
		handlers.PostsCategoryList,
	},
	Route{
		"PostsShow",
		"GET",
		"/posts/{postId}",
		handlers.Posts,
	},
	Route{
		"PostsUpdate",
		"PUT",
		"/posts/{postId}",
		handlers.PostsUpdate,
	},
	Route{
		"PostsCreate",
		"POST",
		"/posts",
		handlers.PostsCreate,
	},
	Route{
		"PostsCreateSlack",
		"GET",
		"/slack/posts",
		handlers.PostsCreateFromSlack,
	},
	Route{
		"PostsDelete",
		"DELETE",
		"/posts/{postId}",
		handlers.PostsDelete,
	},
	Route{
		"CompaniesList",
		"GET",
		"/companies",
		handlers.CompaniesList,
	},
	Route{
		"CompaniesShow",
		"GET",
		"/companies/{companyId}",
		handlers.Companies,
	},
	Route{
		"CompaniesUpdate",
		"PUT",
		"/companies/{companyId}",
		handlers.CompaniesUpdate,
	},
	Route{
		"CompaniesCreate",
		"POST",
		"/companies",
		handlers.CompaniesCreate,
	},
	Route{
		"CompaniesDelete",
		"DELETE",
		"/companies/{companyId}",
		handlers.CompaniesDelete,
	},
}
