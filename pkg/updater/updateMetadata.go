package updater

import (
	"encoding/json"
	"log"
	"techwatch/pkg/models"
	"techwatch/pkg/utils"
)

func UpdateMetaData() {
	db, err := utils.GetDBConnection()

	log.Print("Check ast check metadata")
	results, err := db.Query("SELECT id, title, url, creation_date, metadata, active FROM post WHERE last_check < DATE_SUB(NOW(), INTERVAL 5 MINUTE)")
	if err != nil {
		log.Printf("Cannot query the database %s", err.Error())
		defer db.Close()
		return
	}

	for results.Next() {
		var post models.Post
		var metadataJson []byte
		err = results.Scan(&post.Id, &post.Title, &post.Url, &post.CreationDate, &metadataJson, &post.Active)
		if err != nil {
			log.Println("Cannot retrieve MySQL data", err.Error())
			continue
		}
		models.ScrapeHtmlFromUrl(&post)
		active := 0
		if post.Meta != nil {
			active = 1
		}

		err := json.Unmarshal(metadataJson, &post.Meta)
		if err != nil {
			log.Println("cannot unMarshal metadata", err.Error())
			continue
		}

		insForm, err := db.Prepare("UPDATE post SET metadata=?, active=? where id = ?")
		if err != nil {
			log.Println(err.Error())
			continue
		}
		jsonData, err := json.Marshal(post.Meta)
		insForm.Exec(jsonData, active, post.Id)
	}
	defer db.Close()
}
