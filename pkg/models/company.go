package models

import (
	"database/sql"
)

type Company struct {
	Id           int    `json:"id"`
	Name         string `json:"name"`
	CreationDate string `json:"creation_date"`
	SlackToken   string `json:"-"`
}

type ListCompany struct {
	Elements     []Company `json:"elements"`
	CountElement int       `json:"count_element"`
	CurrentPage  int       `json:"current_page"`
	PerPage      int       `json:"per_page"`
	Sort         string    `json:"sort_by"`
}

func GetCompany(db *sql.DB, companyId string) (Company, error) {
	var company Company
	err := db.QueryRow("SELECT id, name, creation_date FROM company where id = ?", companyId).Scan(&company.Id, &company.Name, &company.CreationDate)

	return company, err
}

func GetCompanyForSlack(db *sql.DB, slackToken string) (Company, error) {
	var company Company
	err := db.QueryRow("SELECT id, name, creation_date FROM company where slack_token = ?", slackToken).Scan(&company.Id, &company.Name, &company.CreationDate)

	return company, err
}
