package models

import "database/sql"

type Category struct {
	Id           int    `json:"id"`
	Label        string `json:"label"`
	CreationDate string `json:"creation_date"`
	CompanyId    string `json:"-"`
}

type ListCategory struct {
	Elements     []Category `json:"elements"`
	CountElement int        `json:"count_element"`
	CurrentPage  int        `json:"current_page"`
	PerPage      int        `json:"per_page"`
	Sort         string     `json:"sort_by"`
}

func GetCategory(db *sql.DB, categoryId string, companyId int) (Category, error) {
	var category Category
	err := db.QueryRow("SELECT id, label, creation_date, company_id FROM category where id = ? AND company_id = ?", categoryId, companyId).Scan(&category.Id, &category.Label, &category.CreationDate, &category.CompanyId)

	return category, err
}
