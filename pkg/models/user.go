package models

import "github.com/dgrijalva/jwt-go"

type User struct {
	Id           string `json:"id"`
	Username     string `json:"username"`
	Password     string `json:"password"`
	CompanyId    int    `json:"company_id"`
	CreationDate int    `json:"creation_date"`
}

type ResponseUser struct {
	Id           string `json:"id"`
	Username     string `json:"username"`
	Password     string `json:"-"`
	CompanyId    int    `json:"company_id"`
	CreationDate int    `json:"creation_date"`
	Token        string `json:"token"`
}

type UserLogin struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Claims struct {
	Username  string `json:"username"`
	CompanyId int    `json:"company_id"`
	jwt.StandardClaims
}

type JWTResponse struct {
	JWToken string `json:"jwt_token"`
}
