package models

import (
	"database/sql"
	"encoding/json"
	"github.com/PuerkitoBio/goquery"
	"log"
	"net/http"
)

type HTMLMetaOg struct {
	Title       string `json:"title"`
	Image       string `json:"image"`
	Description string `json:"description"`
}

type Post struct {
	Id           int         `json:"id"`
	Title        string      `json:"title"`
	Url          string      `json:"url"`
	CreationDate string      `json:"creation_date"`
	CompanyId    string      `json:"-"`
	Categories   []Category  `json:"categories"`
	Meta         *HTMLMetaOg `json:"meta"`
	Active       int         `json:"active"`
}

type CreationPost struct {
	Title        string `json:"title"`
	Url          string `json:"url"`
	Categories   string `json:"tags"`
	CreationDate string `json:"creation_date"`
}

type ListPost struct {
	Elements     []Post `json:"elements"`
	CountElement int    `json:"count_element"`
	CurrentPage  int    `json:"current_page"`
	PerPage      int    `json:"per_page"`
	Sort         string `json:"sort_by"`
}

type SlackResponse struct {
	ResponseType string `json:"response_type"`
	Text         string `json:"text"`
}

func GetPost(db *sql.DB, postId string, companyId int) (Post, error) {
	var post Post
	var metadataJson []byte
	err := db.QueryRow("SELECT id, title, url, creation_date, company_id, metadata, active FROM post where id = ? AND company_id=?", postId, companyId).Scan(&post.Id, &post.Title, &post.Url, &post.CreationDate, &post.CompanyId, &metadataJson, &post.Active)
	if err != nil {
		if err == sql.ErrNoRows {
			log.Printf("no rows for postId %s and companyId %d", postId, companyId)
		}
		return post, err
	}

	err = json.Unmarshal(metadataJson, &post.Meta)
	if err != nil {
		return post, err
	}

	AddCategoryToPost(db, &post)

	return post, nil
}

func AddCategoryToPost(db *sql.DB, post *Post) {
	results, err := db.Query("SELECT c.id, c.label, c.creation_date, c.company_id FROM category c JOIN post_category pc ON pc.category_id = c.id AND pc.post_id = ?", post.Id)
	if err == sql.ErrNoRows {
		return
	}
	if err != nil {
		log.Print(err.Error())
		return
	}

	for results.Next() {
		var category Category
		err = results.Scan(&category.Id, &category.Label, &category.CreationDate, &category.CompanyId)
		if err != nil {
			log.Print(err.Error())
			return
		}
		post.Categories = append(post.Categories, category)
	}
}

func ScrapeHtmlFromUrl(post *Post) {
	// Request the HTML page.
	res, err := http.Get(post.Url)
	if err != nil {
		log.Printf(err.Error())
		return
	}
	defer res.Body.Close()
	if res.StatusCode != 200 {
		log.Printf("url: %s couldn't be reach status code error: %d %s", post.Url, res.StatusCode, res.Status)
		return
	}

	// Load the HTML document
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Printf(err.Error())
		return
	}

	metaOg := HTMLMetaOg{}

	metaOg.Title, _ = doc.Find("meta[property=\"og:title\"]").Attr("content")
	metaOg.Image, _ = doc.Find("meta[property=\"og:image\"]").Attr("content")
	metaOg.Description, _ = doc.Find("meta[property=\"og:description\"]").Attr("content")

	post.Meta = &metaOg
}
