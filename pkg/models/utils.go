package models

type Configuration struct {
	User     string `json:"user"`
	Password string `json:"password"`
	Host     string `json:"host"`
}
