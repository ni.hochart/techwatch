package main

import (
	"github.com/robfig/cron"
	"github.com/rs/cors"
	"log"
	"net/http"
	"techwatch/pkg/routers"
	"techwatch/pkg/updater"
)

func main() {
	router := routers.NewRouter()

	c := cron.New()
	c.AddFunc("0 0 * * * *", updater.UpdateMetaData)
	c.Start()

	handler := cors.AllowAll().Handler(router)
	log.Fatal(http.ListenAndServeTLS(":8443", "server.crt", "server.key", handler))
}
